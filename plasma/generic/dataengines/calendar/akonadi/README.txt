These files have been copied from trunk kdepim/akonadi/kcal and should be kept in sync for bug fixes.

Note that the exports have been removed from the headers, but no other changes have been made.

These classes will be moved from kdepim to kdepimlibs in KDE SC 4.6 and this copy should be deleted when this happens.  At this time it is recommended to move the calendar events dataengine logic into the akonadi dataengine.
